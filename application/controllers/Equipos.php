
<?php
    class Equipos extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("equipo");

      }

      public function index(){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("equipos/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $this->load-> view("header");
        $this->load-> view("equipos/nuevo");
        $this->load-> view("footer");
      }
      public function editar($id_equi){
        $data["listadoEquipos"]=$this->equipo->consultarPorId($id_equi);
        $this->load-> view("header");
        $this->load-> view("equipos/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_equi=$this->input->post("id_equi");
        $datosEquipoEditado=array(
            "nombre_equi"=>$this->input->post("nombre_equi"),
            "categoria_equi"=>$this->input->post("categoria_equi")
          );

        if ($this->equipo->actualizar($id_equi,$datosEquipoEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("equipos/index");
        }
        else {
          echo "ERROR AL INSERTAR";
        }
      }


      public function guardarEquipo(){
        $datosNuevoEquipo=array(
          "nombre_equi"=>$this->input->post("nombre_equi"),
          "categoria_equi"=>$this->input->post("categoria_equi")
        );
      if ($this->equipo->insertar($datosNuevoEquipo)) {
        $this->session->set_flashdata("confirmacion","Equipo insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");
      }
        redirect("equipos/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_equi){

        if($this->equipo->eliminar($id_equi)){
          redirect("equipos/index");
        }else{
          echo "ERROR AL ELIMINAR";
        }
    }
} //Cierre de la clase
?>
