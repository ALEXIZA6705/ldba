
<?php
    class Jugadores extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("jugador");
        $this->load->model("equipo");
      }

      public function index(){
        $data["listadoJugadores"]=$this->jugador->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("jugadores/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("jugadores/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_jug){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $data["jugador"]=$this->jugador->consultarPorId($id_jug);
        $this->load-> view("header");
        $this->load-> view("jugadores/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_jug=$this->input->post("id_jug");
        $datosJugadorEditado=array(
            "nombre_jug"=>$this->input->post("nombre_jug"),
            "apellido_jug"=>$this->input->post("apellido_jug"),
            "identificacion_jug"=>$this->input->post("identificacion_jug"),
            "numero_jug"=>$this->input->post("numero_jug"),
            "estado_jug"=>$this->input->post("estado_jug"),
            "goles_jug"=>$this->input->post("goles_jug"),
            "perfil_jug"=>$this->input->post("perfil_jug"),
            "email_jug"=>$this->input->post("email_jug"),
            "password_jug"=>$this->input->post("password_jug"),
            "fk_id_equi"=>$this->input->post("fk_id_equi")
          );
        if ($this->jugador->actualizar($id_jug,$datosJugadorEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("jugadores/index");
        }
        else {
          echo "ERROR AL INSERTAR";
        }
      }

//-
      public function guardarJugador(){
        $datosNuevoJugador=array(
          "nombre_jug"=>$this->input->post("nombre_jug"),
          "apellido_jug"=>$this->input->post("apellido_jug"),
          "identificacion_jug"=>$this->input->post("identificacion_jug"),
          "numero_jug"=>$this->input->post("numero_jug"),
          "estado_jug"=>$this->input->post("estado_jug"),
          "goles_jug"=>$this->input->post("goles_jug"),
          "perfil_jug"=>$this->input->post("perfil_jug"),
          "email_jug"=>$this->input->post("email_jug"),
          "password_jug"=>$this->input->post("password_jug"),
          "fk_id_equi"=>$this->input->post("fk_id_equi")
        );

      if ($this->jugador->insertar($datosNuevoJugador)) {
        $this->session->set_flashdata("confirmacion","Jugador insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente");
      }
        redirect("jugadores/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_jug){
      if($this->jugador->eliminar($id_jug)){
        redirect("jugadores/index");
      }else{
        echo "ERROR AL ELIMINAR";
      }
    }
} //Cierre de la clase
?>
