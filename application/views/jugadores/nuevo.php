<center>
<form class="" action="<?php echo site_url();?>/jugadores/guardarJugador" method="post" id="frm_nuevo_jugador">
  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;" type="table">
    <h4> <b>AGREGAR JUGADOR</b> </h4> <br>
    <label for="">EQUIPO</label>
    <select style="color:#000000" class="form-control"   name="fk_id_equi" id="fk_id_equi" required>
      <option value="">--Seleccione un equipo--</option>
      <?php if ($listadoEquipos): ?>
        <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
          <option value="<?php echo $equipoTemporal->id_equi; ?>">
            <?php echo $equipoTemporal->nombre_equi; ?>
          </option>
        <?php endforeach; ?>
      <?php endif; ?>
    </select>
    <br>
    <br>
    <label for="">Nombre: </label>
    <input class="form-control"  required="requiered" onkeypress="return validar(event)"  type="text" name="nombre_jug" id='nombre_jug' value="" placeholder="Ingrese el nombre">
    <br>
    <label for="">Apellido</label>
    <br>
    <input class="form-control" required="requiered" onkeypress="return validar(event)" type="text" name="apellido_jug" id="apellido_jug" value="" placeholder="Ingrese el apellido">
    <br>
    <label for="">Identificacion</label> <br>
    <input class="form-control"  required="requiered"  type="number" name="identificacion_jug" id="identificacion_jug" value="" placeholder="Ingrese su identificacion">
    <br>
    <label for="">Numero</label>
    <br>
    <input class="form-control"  required="requiered"  type="number" name="numero_jug" id="numero_jug" value="" placeholder="Ingrese numero de camiseta">
    <br>
    <label for="" >Estado: </label>
    <select style="color:#000000" class="form-control" type="text" required="requiered"  name="estado_jug" id=estado_jug value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="NACIONAL" >NACIONAL</option>
      <option value="FORANEO">FORANEO</option>
    </select>
    <br>
    <label for="">GOLES: </label>
    <br>
    <input class="form-control"  required="requiered" type="number" name="goles_jug" id='goles_jug' value="" placeholder="Ingrese el número de goles">
    <br>
    <label for="" >PERFIL: </label>
    <select style="color:#000000" class="form-control" type="text" required="requiered"  name="perfil_jug" id=perfil_jug value="" placeholder="Seleccione su perfil">
      <option value="">--Seleccione--</option>
      <option value="ADMINISTRADOR" >ADMINISTRADOR</option>
      <option value="DELEGADO">DELEGADO</option>
      <option value="JUGADOR">JUGADOR</option>
    </select>
    <br>
    <label for="">Email: </label>
    <br>
    <input class="form-control" required="requiered" type="text" name="email_jug" id='email_jug' value="" placeholder="Ingrese el correo electrónico">
    <br>
    <label for="">CONTRASEÑA: </label>
    <br>
    <input class="form-control" required="requiered" onkeypress="return validar(event)" type="text" name="password_jug" id='password_jug' value="" placeholder="ingrese la contraseña">
    <br>
    <br>
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    <a href="<?php echo site_url(); ?>/jugadores/index"
      class="btn btn-warning"><i class="fa fa-times"></i>
      CANCELAR
    </a>
    <center>
</div>
</form>
</center>


<!-- AGREGAMOS TAG PARA LLAMAR AL FORMULARIO -->
<script type="text/javascript">
    $("#frm_nuevo_jugador").validate({
      rules:{
        fk_id_equi:{
          required:true
        },

        apellido_jug:{
          letras:true,
          required:true
        },
        nombre_jug:{
          letras:true,
          required:true
        },
        identificacion_jug:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        numero_jug:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },
        estado_jug:{
          required:true
        },
        goles_jug:{
          required:true
        },
        perfil_jug:{
          required:true
        },
        email_jug:{
          required:true
        },
        password_jug:{
          required:true,
          minlength:6
        },
      },

      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        nombre_jug:{
          letras:"El nombre solo debe tener letras",
          required:"Por favor ingrese el nombre"
        },
        apellido_jug:{
          letras:"El apellido solo debe tener letras",
          required:"Por favor ingrese el apellido"
        },
        identificacion_jug:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La camiseta solo acepta números"
        },
        numero_jug:{
          required:"Por favor ingrese el número de teléfono celular",
          minlength:"El número de camiseta debe tener mínimo 1 digito",
          maxlength:"El número de celular debe tener máximo 3 digitos",
          digits:"El número de celular solo acepta números"
        },
        estado_jug:{
          required:"Por favor seleccione el estado"
        },
        perfil_jug:{
          required:"Por favor seleccione un perfil"
        },
        email_jug:{
          required:"Por favor ingrese una direccion de correo electrónico"
        },
        password_jug:{
          required:"Por favor introduzca una contraseña"
        },

      }
    });
</script>
