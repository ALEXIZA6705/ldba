<center>
<form class="" action="<?php echo site_url();?>/jugadores/procesarActualizacion" method="post" >
  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;">
    <h4> <b>ACTUALIZAR DATOS DE JUGADORES</b> </h4> <br>
    <input type="hidden" name="id_jug" id="id_jug" value="<?php echo $jugador->id_jug; ?>">
    <br>
    <label for="">EQUIPO</label>
    <select style="color:#000000" class="form-control"   name="fk_id_equi" id="fk_id_equi" required>
      <option value="">--Seleccione un equipo--</option>
      <?php if ($listadoEquipos): ?>
        <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
          <option value="<?php echo $equipoTemporal->id_equi; ?>">
            <?php echo $equipoTemporal->nombre_equi; ?>
          </option>
        <?php endforeach; ?>
      <?php endif; ?>
    </select>
    <br>
    <label for="">Nombre: </label>
    <input class="form-control" value="<?php echo $jugador->nombre_jug; ?>" required="requiered" onkeypress="return validar(event)"  type="text" name="nombre_jug" id='nombre_jug' value="" placeholder="Ingrese el nombre">
    <br>
    <label for="">Apellido</label>
    <br>
    <input class="form-control" value="<?php echo $jugador->apellido_jug; ?>" required="requiered" onkeypress="return validar(event)" type="text" name="apellido_jug" id="apellido_jug" value="" placeholder="Ingrese el apellido">
    <br>
    <label for="">Identificacion</label> <br>
    <input class="form-control" value="<?php echo $jugador->identificacion_jug; ?>" required="requiered"  type="number" name="identificacion_jug" id="identificacion_jug" value="" placeholder="Ingrese su identificacion">
    <br>
    <label for="">Numero</label> <br>
    <input class="form-control" value="<?php echo $jugador->numero_jug; ?>" required="requiered"  type="number" name="numero_jug" id="numero_jug" value="" placeholder="Ingrese numero de camiseta">
    <br>
    <label for="" >Estado: </label>
    <select style="color:#000000" class="form-control"  required="requiered"  name="estado_jug" id=estado_jug value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="NACIONAL" >NACIONAL</option>
      <option value="FORANEO">FORANEO</option>
    </select>
    <br>
    <label for="">GOLES: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->goles_jug; ?>" required="requiered" type="number" name="goles_jug" id='goles_jug' value="" placeholder="Ingrese el número de teléfono">
    <br>
    <label for="" >PERFIL: </label>
    <select style="color:#000000" class="form-control"  required="requiered"  name="perfil_jug" id=perfil_jug value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="USUARIO" >USUARIO</option>
      <option value="DELEGADO">DELEGADO</option>
      <option value="JUGADOR">JUGADOR</option>
    </select>
    <label for="">Email: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->email_jug; ?>" required="requiered" type="text" name="email_jug" id='email_jug' value="" placeholder="Ingrese el correo electrónico">
    <br>
    <label for="">CONTRASEÑA: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->password_jug; ?>" required="requiered" onkeypress="return validar(event)" type="text" name="password_jug" id='password_jug' value="" placeholder="actualice la contraseña">
    <br><br>
  <button type="submit" name="button" class="btn btn-primary">ACTUALIZAR</button>
    <a href="<?php echo site_url(); ?>/jugadores/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</div>
</form>
</center>


<!-- ************************************ACTIVANDO EL equipo SELECCIONADO POR EL JUGADOR*********************************** -->
<script type="text/javascript">
  $("#fk_id_equi").val("<?php echo $jugador->fk_id_equi; ?>");
  $("#estado_jug").val("<?php echo $jugador->estado_jug; ?>");
</script>
