<br>
<center>
  <h2>LISTADO DE LOS JUGADORES</h2>

</center>
<hr>
<center>
  <a href="<?php echo site_url(); ?>/jugadores/nuevo"> <i class="fa fa-plus-circle fa-lg"></i>Agregar nuevo</a>
  <br>
</center>
      <?php if ($listadoJugadores): ?>
        <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">IDENTIFICACION</th>
            <th class="text-center">NUMERO</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">GOLES</th>
            <th class="text-center">PERFIL</th>
            <th class="text-center">EQUIPO</th>
            <th class="text-center">EMAIL</th>
            <th class="text-center">CONTRASEÑA</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoJugadores->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->apellido_jug;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_jug;?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->identificacion_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->numero_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->estado_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->goles_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->perfil_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->nombre_equi;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->email_jug;?>
              </td>
              <td class="text-center">
              <?php echo $filaTemporal->password_jug;?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/jugadores/editar/<?php echo $filaTemporal->id_jug;?>" class="btn btn-warning">Editar</a>
                <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/jugadores/procesarEliminacion/<?php echo $filaTemporal->id_jug;?>" class="btn btn-danger">ELIMINAR</a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <div class="alert alert-damger">
        <h3>NO SE ENCONTRARON JUGADORES REGISTRADOS</h3>
      </div>
    <?php endif; ?>


    <script type="text/javascript">
      function confirmarEliminacion(id_jug){
            iziToast.question({
                timeout: 20000,
                close: false,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'CONFIRMACIÓN',
                message: '¿Esta seguro de eliminar el jugador de forma pernante?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        window.location.href=
                        "<?php echo site_url(); ?>/jugadores/procesarEliminacion/"+id_jug;

                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }],
                ]
            });
      }
  </script>
