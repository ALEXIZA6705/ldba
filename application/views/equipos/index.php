<br>
<center>
  <h2>REGISTRO DE EQUIPOS</h2>

</center>
<br>
<center>
    <a href="<?php echo site_url(); ?>/equipos/nuevo" class="btn btn-secondary">
      Agregar Equipo
    </a>
    <br>
    <br>
</center>

<?php if ($listadoEquipos): ?>
  <table class="table table-bordered table-striped table-hover ">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">CATEGORIA</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoEquipos->result() as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_equi;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_equi;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->categoria_equi;?>
        </td>
        <td class="text-center">
          <a href="<?php echo site_url(); ?>/equipos/editar/<?php echo $filaTemporal->id_equi;?>" class="btn btn-warning">Editar</a>
          <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/equipos/procesarEliminacion/<?php echo $filaTemporal->id_equi;?>" class="btn btn-danger">ELIMINAR</a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

    <tfoot>

    </tfoot>
  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron equipos por el momento</h3>

  </div>

<?php endif; ?>
