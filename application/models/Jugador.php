<?php
class Jugador extends CI_Model{//
      public function __construct(){
        parent::__construct();
      }
      // FUNCION PARA INSERTAR
      public function insertar($datos){
        return $this->db->insert('jugador',$datos);
      }
      // FUNCION PARA ACTUALIZAR
      public function actualizar($id_jug,$datos){
        $this->db->where("id_jug",$id_jug);
        return $this->db->update('jugador',$datos);
      }
      //FUNCION PARA SACAR EL DETALLE DE UN jugador
      public function consultarPorId($id_jug){
        $this->db->where("id_jug",$id_jug);
        $jugador=$this->db->join("equipo","equipo.id_equi=jugador.fk_id_equi");
        $jugador=$this->db->get("jugador");
          if($jugador->num_rows()>0){
            return $jugador->row();//cuando SI hay jugador
          }else{
            return false;//cuando NO hay jugador
          }
        }
      // FUNCION PARA CONSULTAR TODOS LOS jugadores
      public function consultarTodos(){
      $this->db->join("equipo","equipo.id_equi=jugador.fk_id_equi");

      $listadoJugadores=$this->db->get('jugador');
      if ($listadoJugadores->num_rows()>0) {
        // Cuando hay paises
        return $listadoJugadores;
      }else {
        // Cuando no hay paises
        return false;
      }
    }
      // FUNCIÒN PARA ELIMINAR
      public function eliminar($id_jug){
        $this->db->where("id_jug",$id_jug);
        return $this->db->delete("jugador");
      }
  }
 ?>
