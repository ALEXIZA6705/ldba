<?php
    class Usuario extends CI_Model{
      public function __construct(){
        parent::__construct();
      }

      //consulta para el Login de Usuario
      public function buscarUsuarioPorEmailPassword($email_usu,
                      $password_usu){
            $this->db->where("email_usu",$email_usu);
            $this->db->where("password_usu",$password_usu);
            $usuarioEncontrado=$this->db->get("usuario");
            if($usuarioEncontrado->num_rows()>0){
              return $usuarioEncontrado->row();
            }else{//cuando las credenciales estan incorrectas
              return false;
            }
      }
      public function insertarUsuario($data){
        return $this->db->insert("usuario",$data);

      }
      public function obtenerTodos(){
        $listado=$this->db->get("usuario");
        if($listado->num_rows()>0){
          return $listado;
        }else{//cuando las credenciales estan incorrectas
          return false;
        }

      }
      public function eliminar($ide_usu){
      $this->db->where("ide_usu",$ide_usu);
    return  $this->db->delete("usuario");
    }

    }//cierre de la clase Usuario
